import 'package:json_annotation/json_annotation.dart';

part 'picture.g.dart';

@JsonSerializable(fieldRename: FieldRename.snake, nullable: true, explicitToJson: true)
class Picture {
  final String small;
  final String medium;
  final String large;

  Picture({this.small, this.medium, this.large});

  factory Picture.fromJson(Map<String, dynamic> json) =>
      _$PictureFromJson(json);

  Map<String, dynamic> toJson() => _$PictureToJson(this);
}
