import 'package:json_annotation/json_annotation.dart';
import './picture.dart';
import './region.dart';
import './privacy.dart';

import './legacy_picture.dart';

part 'user.g.dart';

@JsonSerializable(
    fieldRename: FieldRename.snake, nullable: true, explicitToJson: true)
class User {
  final String id;
  final String name;
  final String description;
  final bool isPublisher;
  final bool isSubscriber;
  final String email;
  final String phone;
  final String tagline;
  final String publisherDescription;
  final String publicUrl;
  final String shareUrl;
  final Picture avatar;
  final int publisherFollowerCount;
  final int totalPlays;
  final int totalFollows;
  final int milestone;
  final String username;
  @JsonKey(name: "picture")
  final LegacyPicture legacyPicture;
  final String website;
  final Region region;
  @JsonKey(name: "search_result_type")
  final String searchResultType;
  final Privacy privacy;

  User(
      {this.id,
      this.name,
      this.description,
      this.isPublisher,
      this.isSubscriber,
      this.email,
      this.phone,
      this.tagline,
      this.publisherDescription,
      this.publicUrl,
      this.shareUrl,
      this.avatar,
      this.publisherFollowerCount,
      this.totalPlays,
      this.totalFollows,
      this.region,
      this.milestone,
      this.username,
      this.website,
      this.legacyPicture,
      this.searchResultType,
      this.privacy});

  factory User.fromJson(Map<String, dynamic> json) => _$UserFromJson(json);

  Map<String, dynamic> toJson() => _$UserToJson(this);
}
