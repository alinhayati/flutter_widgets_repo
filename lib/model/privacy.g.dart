// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'privacy.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Privacy _$PrivacyFromJson(Map<String, dynamic> json) {
  return Privacy(
      account_type: json['account_type'] as String,
      follow_requests: json['follow_requests'] as String,
      direct_message: json['direct_message'] as String,
      status_and_charts: json['status_and_charts'] as String,
      thanks_for_meditating: json['thanks_for_meditating'] as String);
}

Map<String, dynamic> _$PrivacyToJson(Privacy instance) => <String, dynamic>{
      'account_type': instance.account_type,
      'follow_requests': instance.follow_requests,
      'direct_message': instance.direct_message,
      'status_and_charts': instance.status_and_charts,
      'thanks_for_meditating': instance.thanks_for_meditating
    };
