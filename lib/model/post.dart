import 'package:json_annotation/json_annotation.dart';
import './user.dart';

part 'post.g.dart';

@JsonSerializable(fieldRename: FieldRename.snake, nullable: true, explicitToJson: true)
class Post {
  final User author;
  final String message;

  // ignore: non_constant_identifier_names
  final int total_replies;
  // ignore: non_constant_identifier_names
  final int total_publisher_audio_reply;
  // ignore: non_constant_identifier_names
  final int total_likes;
  // ignore: non_constant_identifier_names
  final int total_reply_count;

  Post(
      {this.author,
      this.message,
      // ignore: non_constant_identifier_names
      this.total_replies,
      // ignore: non_constant_identifier_names
      this.total_publisher_audio_reply,
      // ignore: non_constant_identifier_names
      this.total_likes,
      // ignore: non_constant_identifier_names
      this.total_reply_count});

  factory Post.fromJson(Map<String, dynamic> json) => _$PostFromJson(json);

  Map<String, dynamic> toJson() => _$PostToJson(this);
}
