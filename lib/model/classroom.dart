import 'package:json_annotation/json_annotation.dart';
import './post.dart';

part 'classroom.g.dart';

@JsonSerializable(fieldRename: FieldRename.snake, nullable: true, explicitToJson: true)
class Classroom {
  // ignore: non_constant_identifier_names
  final int total_post_count;
  // ignore: non_constant_identifier_names
  final int total_posts_and_replies;
  // ignore: non_constant_identifier_names
  final List<Post> publisher_posts;

  Classroom(
      // ignore: non_constant_identifier_names
      {this.total_post_count,
      // ignore: non_constant_identifier_names
      this.total_posts_and_replies,
      // ignore: non_constant_identifier_names
      this.publisher_posts});

  factory Classroom.fromJson(Map<String, dynamic> json) =>
      _$ClassroomFromJson(json);

  Map<String, dynamic> toJson() => _$ClassroomToJson(this);
}
