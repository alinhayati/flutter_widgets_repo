import 'package:json_annotation/json_annotation.dart';

part 'legacy_picture.g.dart';

@JsonSerializable(fieldRename: FieldRename.snake, nullable: true, explicitToJson: true)
class LegacyPicture {
  final String path;

  LegacyPicture({this.path});

  factory LegacyPicture.fromJson(Map<String, dynamic> json) => _$LegacyPictureFromJson(json);

  Map<String, dynamic> toJson() => _$LegacyPictureToJson(this);
}
