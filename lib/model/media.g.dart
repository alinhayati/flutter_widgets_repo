// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'media.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Media _$MediaFromJson(Map<String, dynamic> json) {
  return Media(
      path: json['path'] as String,
      length: (json['length'] as num)?.toDouble());
}

Map<String, dynamic> _$MediaToJson(Media instance) =>
    <String, dynamic>{'path': instance.path, 'length': instance.length};
