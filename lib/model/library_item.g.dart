// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'library_item.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

LibraryItem _$LibraryItemFromJson(Map<String, dynamic> json) {
  return LibraryItem(
      id: json['id'] as String,
      title: json['title'] as String,
      picture: json['picture'] == null
          ? null
          : Picture.fromJson(json['picture'] as Map<String, dynamic>),
      content: json['content'] as String,
      publisher: json['publisher'] == null
          ? null
          : User.fromJson(json['publisher'] as Map<String, dynamic>),
      classroom: json['classroom'] == null
          ? null
          : Classroom.fromJson(json['classroom'] as Map<String, dynamic>),
      media: (json['media'] as List)
          ?.map((e) =>
              e == null ? null : Media.fromJson(e as Map<String, dynamic>))
          ?.toList(),
      short_description: json['short_description'] as String,
      rating_score: (json['rating_score'] as num)?.toDouble(),
      content_type: json['content_type'] as String,
      long_description: json['long_description'] as String,
      media_length: json['media_length'] as int,
      play_count: json['play_count'] as int,
      standard_media_paths: (json['standard_media_paths'] as List)
          ?.map((e) => e as String)
          ?.toList());
}

Map<String, dynamic> _$LibraryItemToJson(LibraryItem instance) =>
    <String, dynamic>{
      'id': instance.id,
      'title': instance.title,
      'content': instance.content,
      'publisher': instance.publisher?.toJson(),
      'picture': instance.picture?.toJson(),
      'classroom': instance.classroom?.toJson(),
      'media': instance.media?.map((e) => e?.toJson())?.toList(),
      'content_type': instance.content_type,
      'short_description': instance.short_description,
      'long_description': instance.long_description,
      'rating_score': instance.rating_score,
      'play_count': instance.play_count,
      'media_length': instance.media_length,
      'standard_media_paths': instance.standard_media_paths
    };
