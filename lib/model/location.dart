import 'package:json_annotation/json_annotation.dart';

part 'location.g.dart';

@JsonSerializable(fieldRename: FieldRename.snake, nullable: true, explicitToJson: true)
class Location {
   double lat;
   double lon;
   double x;
   double y;

  Location({this.lat, this.lon});

  factory Location.fromJson(Map<String, dynamic> json) => _$LocationFromJson(json);
  
  Map<String, dynamic> toJson() => _$LocationToJson(this);

}
