import './picture.dart';
import './user.dart';
import 'package:json_annotation/json_annotation.dart';

import './classroom.dart';
import './media.dart';

part 'library_item.g.dart';

@JsonSerializable(
    fieldRename: FieldRename.snake, nullable: true, explicitToJson: true)
class LibraryItem {
  final String id;
  final String title;
  final String content;
  final User publisher;
  final Picture picture;
  final Classroom classroom;
  final List<Media> media;

  // ignore: non_constant_identifier_names
  final String content_type;
  // ignore: non_constant_identifier_names
  final String short_description;
  // ignore: non_constant_identifier_names
  final String long_description;
  // ignore: non_constant_identifier_names
  final double rating_score;
  // ignore: non_constant_identifier_names
  final int play_count;
  // ignore: non_constant_identifier_names
  final int media_length;
  // ignore: non_constant_identifier_names
  final List<String> standard_media_paths;

  LibraryItem(
      {this.id,
      this.title,
      this.picture,
      this.content,
      this.publisher,
      this.classroom,
      this.media,
      // ignore: non_constant_identifier_names
      this.short_description,
      // ignore: non_constant_identifier_names
      this.rating_score,
      // ignore: non_constant_identifier_names
      this.content_type,
      // ignore: non_constant_identifier_names
      this.long_description,
      // ignore: non_constant_identifier_names
      this.media_length,
      // ignore: non_constant_identifier_names
      this.play_count,
      // ignore: non_constant_identifier_names
      this.standard_media_paths});

  factory LibraryItem.fromJson(Map<String, dynamic> json) =>
      _$LibraryItemFromJson(json);

  Map<String, dynamic> toJson() => _$LibraryItemToJson(this);
}
