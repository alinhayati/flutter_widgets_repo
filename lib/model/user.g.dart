// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

User _$UserFromJson(Map<String, dynamic> json) {
  return User(
      id: json['id'] as String,
      name: json['name'] as String,
      description: json['description'] as String,
      isPublisher: json['is_publisher'] as bool,
      isSubscriber: json['is_subscriber'] as bool,
      email: json['email'] as String,
      phone: json['phone'] as String,
      tagline: json['tagline'] as String,
      publisherDescription: json['publisher_description'] as String,
      publicUrl: json['public_url'] as String,
      shareUrl: json['share_url'] as String,
      avatar: json['avatar'] == null
          ? null
          : Picture.fromJson(json['avatar'] as Map<String, dynamic>),
      publisherFollowerCount: json['publisher_follower_count'] as int,
      totalPlays: json['total_plays'] as int,
      totalFollows: json['total_follows'] as int,
      region: json['region'] == null
          ? null
          : Region.fromJson(json['region'] as Map<String, dynamic>),
      milestone: json['milestone'] as int,
      username: json['username'] as String,
      website: json['website'] as String,
      legacyPicture: json['picture'] == null
          ? null
          : LegacyPicture.fromJson(json['picture'] as Map<String, dynamic>),
      searchResultType: json['search_result_type'] as String,
      privacy: json['privacy'] == null
          ? null
          : Privacy.fromJson(json['privacy'] as Map<String, dynamic>));
}

Map<String, dynamic> _$UserToJson(User instance) => <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'description': instance.description,
      'is_publisher': instance.isPublisher,
      'is_subscriber': instance.isSubscriber,
      'email': instance.email,
      'phone': instance.phone,
      'tagline': instance.tagline,
      'publisher_description': instance.publisherDescription,
      'public_url': instance.publicUrl,
      'share_url': instance.shareUrl,
      'avatar': instance.avatar?.toJson(),
      'publisher_follower_count': instance.publisherFollowerCount,
      'total_plays': instance.totalPlays,
      'total_follows': instance.totalFollows,
      'milestone': instance.milestone,
      'username': instance.username,
      'picture': instance.legacyPicture?.toJson(),
      'website': instance.website,
      'region': instance.region?.toJson(),
      'search_result_type': instance.searchResultType,
      'privacy': instance.privacy?.toJson()
    };
