// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'post.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Post _$PostFromJson(Map<String, dynamic> json) {
  return Post(
      author: json['author'] == null
          ? null
          : User.fromJson(json['author'] as Map<String, dynamic>),
      message: json['message'] as String,
      total_replies: json['total_replies'] as int,
      total_publisher_audio_reply: json['total_publisher_audio_reply'] as int,
      total_likes: json['total_likes'] as int,
      total_reply_count: json['total_reply_count'] as int);
}

Map<String, dynamic> _$PostToJson(Post instance) => <String, dynamic>{
      'author': instance.author?.toJson(),
      'message': instance.message,
      'total_replies': instance.total_replies,
      'total_publisher_audio_reply': instance.total_publisher_audio_reply,
      'total_likes': instance.total_likes,
      'total_reply_count': instance.total_reply_count
    };
