import 'package:json_annotation/json_annotation.dart';
import './location.dart';

part 'region.g.dart';


@JsonSerializable(fieldRename: FieldRename.snake, nullable: true, explicitToJson: true)
class Region {
  final String name;
  final Location location;

  Region({this.name, this.location});

  factory Region.fromJson(Map<String, dynamic> json) => _$RegionFromJson(json);
  
  Map<String, dynamic> toJson() => _$RegionToJson(this);
}
