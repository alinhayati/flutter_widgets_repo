import 'package:json_annotation/json_annotation.dart';

part 'media.g.dart';

@JsonSerializable(fieldRename: FieldRename.snake, nullable: true, explicitToJson: true)
class Media {
  final String path;
  final double length;

  Media({this.path, this.length});

  factory Media.fromJson(Map<String, dynamic> json) => _$MediaFromJson(json);

  Map<String, dynamic> toJson() => _$MediaToJson(this);
}
