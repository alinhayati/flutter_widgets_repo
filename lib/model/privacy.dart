import 'package:json_annotation/json_annotation.dart';

part 'privacy.g.dart';

@JsonSerializable(
    fieldRename: FieldRename.snake, nullable: true, explicitToJson: true)
class Privacy {
  // ignore: non_constant_identifier_names
  final String account_type;
  // ignore: non_constant_identifier_names
  final String follow_requests;
  // ignore: non_constant_identifier_names
  final String direct_message;
  // ignore: non_constant_identifier_names
  final String status_and_charts;
  // ignore: non_constant_identifier_names
  final String thanks_for_meditating;

  Privacy(
      // ignore: non_constant_identifier_names
      {this.account_type,
      // ignore: non_constant_identifier_names
      this.follow_requests,
      // ignore: non_constant_identifier_names
      this.direct_message,
      // ignore: non_constant_identifier_names
      this.status_and_charts,
      // ignore: non_constant_identifier_names
      this.thanks_for_meditating});

  factory Privacy.fromJson(Map<String, dynamic> json) =>
      _$PrivacyFromJson(json);

  Map<String, dynamic> toJson() => _$PrivacyToJson(this);
}
