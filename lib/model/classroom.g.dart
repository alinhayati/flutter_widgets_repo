// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'classroom.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Classroom _$ClassroomFromJson(Map<String, dynamic> json) {
  return Classroom(
      total_post_count: json['total_post_count'] as int,
      total_posts_and_replies: json['total_posts_and_replies'] as int,
      publisher_posts: (json['publisher_posts'] as List)
          ?.map((e) =>
              e == null ? null : Post.fromJson(e as Map<String, dynamic>))
          ?.toList());
}

Map<String, dynamic> _$ClassroomToJson(Classroom instance) => <String, dynamic>{
      'total_post_count': instance.total_post_count,
      'total_posts_and_replies': instance.total_posts_and_replies,
      'publisher_posts':
          instance.publisher_posts?.map((e) => e?.toJson())?.toList()
    };
