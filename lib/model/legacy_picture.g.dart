// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'legacy_picture.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

LegacyPicture _$LegacyPictureFromJson(Map<String, dynamic> json) {
  return LegacyPicture(path: json['path'] as String);
}

Map<String, dynamic> _$LegacyPictureToJson(LegacyPicture instance) =>
    <String, dynamic>{'path': instance.path};
