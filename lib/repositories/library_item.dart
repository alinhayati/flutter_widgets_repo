import 'dart:convert';
import 'package:rxdart/rxdart.dart';
import 'package:http/http.dart' as http;

import '../model/library_item.dart';

class LibraryItemRepository {
  Future<int> getFilteredItemsCount(LibraryFilter filter) {
    String url =
        'https://insight-timer-api.firebaseapp.com/apiLibraryItemFilter/request';

    url = url + '?' + formatFilterRequestQuery(filter: filter);
    return http.head(url).then((response) {
      if (response.statusCode == 200) {
        return int.parse(response.headers['x-total-count'].toString());
      } else {
        throw Exception(
            'Failed to load new today ${response.statusCode}: ${response.body}');
      }
    });
  }

  Future<LibraryItemResult> getFilteredItems(LibraryFilter filter) {
    String url =
        'https://insight-timer-api.firebaseapp.com/apiLibraryItemFilter/request';

    url = url + '?' + formatFilterRequestQuery(filter: filter);
    return http.get(url).then((response) {
      if (response.statusCode == 200) {
        int total = int.parse(response.headers['x-total-count'].toString());
        Map apiResp = json.decode(utf8.decode(response.bodyBytes));
        List<LibraryItem> items = apiResp["result"]
            .map<LibraryItem>((item) => LibraryItem.fromJson(item))
            .toList();
        return LibraryItemResult(filter: filter, items: items, total: total);
      } else {
        throw Exception(
            'Failed to load new today ${response.statusCode}: ${response.body}');
      }
    });
  }

  String formatFilterRequestQuery({
    LibraryFilter filter,
  }) {
    Map<String, String> queries = Map<String, String>();
    if (filter.offset != null) {
      queries['from'] = filter.offset.toString();
    }
    if (filter.limit != null) {
      queries['size'] = filter.limit.toString();
    }
    if (filter.sorting != null) {
      String sortOption;
      switch (filter.sorting) {
        case LibraryFilterSorting.mostPlayed:
          sortOption = 'most_played';
          break;
        case LibraryFilterSorting.highestRated:
          sortOption = '"highest_rated"';
          break;
        case LibraryFilterSorting.newest:
          sortOption = 'newest';
          break;
        case LibraryFilterSorting.shortest:
          sortOption = 'shortest';
          break;
        case LibraryFilterSorting.longest:
          sortOption = 'longest';
          break;
      }
      queries['sort_option'] = sortOption;
    }
    if (filter.hasBackgroundMusic != null) {
      queries['has_background_music'] = filter.hasBackgroundMusic.toString();
    }
    if (filter.isReligious != null) {
      queries['is_religious'] = filter.isReligious.toString();
    }
    if (filter.isSpiritual != null) {
      queries['is_spiritual'] = filter.isReligious.toString();
    }
    if (filter.types != null && filter.types.length > 0) {
      queries['content_types'] = filter.types.join(',').toLowerCase();
    }
    if (filter.language != null) {
      queries['device_lang'] = filter.language;
    }
    if (filter.contentLanguages != null && filter.contentLanguages.length > 0) {
      queries['content_langs'] = filter.contentLanguages.join(',');
    }
    if (filter.topics != null && filter.topics.length > 0) {
      queries['content_langs'] = filter.topics.join(',').toLowerCase();
    }
    switch (filter.range) {
      case LibraryFilterRange.zeroFive:
        queries['length_option'] = '0_5';
        break;
      case LibraryFilterRange.sixTen:
        queries['length_option'] = '5_10';
        break;
      case LibraryFilterRange.elevenFifteen:
        queries['length_option'] = '10_15';
        break;
      case LibraryFilterRange.sixteenTwenty:
        queries['length_option'] = '15_20';
        break;
      case LibraryFilterRange.twentyoneThirty:
        queries['length_option'] = '20_30';
        break;
      case LibraryFilterRange.thirtyPlus:
        queries['length_option'] = '30_';
        break;
      default:
        break;
    }

    if (filter.genres != null && !filter.genres.isEmpty) {
      List<String> genres = filter.genres.map((genre) {
        switch (genre) {
          case LibraryFilterGenre.classical:
            return 'CLASSICAL_MUSIC';
          case LibraryFilterGenre.chantingAndMantras:
            return 'CHANTING_AND_MANTRAS';
          case LibraryFilterGenre.soundHealing:
            return 'SOUND_HEALING';
          case LibraryFilterGenre.drumming:
            return 'DRUMMING';
          case LibraryFilterGenre.natureSounds:
            return 'NATURE_SOUNDS';
          case LibraryFilterGenre.instrumental:
            return 'INSTRUMENTAL_MUSIC';
          case LibraryFilterGenre.ambientMusic:
            return 'AMBIENT_MUSIC';
          case LibraryFilterGenre.binauralBeats:
            return 'BINAURAL_BEATS';
        }
      }).toList();
      queries['genres'] = genres.join(',');
    }

    if (filter.voiceGender != null) {
      switch (filter.voiceGender) {
        case LibraryFilterAudioGender.male:
          queries['voice_gender'] = "MALE";
          break;
        case LibraryFilterAudioGender.female:
          queries['voice_gender'] = "FEMALE";
          break;
      }
    }

    List<String> formattedQuery = [];
    queries.forEach((key, val) {
      formattedQuery.add(key + '=' + val);
    });
    return formattedQuery.join('&');
  }

  Observable<LibraryItem> getTodayDailyInsightItem() {
    return Observable.fromFuture(http.get(
            'https://us-central1-insight-timer-a1ac7.cloudfunctions.net/apiDailyInsightGet/request/dailyinsights/2019-01-06'))
        .map((response) {
      if (response.statusCode == 200) {
        return LibraryItem.fromJson(json.decode(response.body));
      } else {
        throw Exception(
            'Failed to load daily insight ${response.statusCode}: ${response.body}');
      }
    });
  }

  Observable<List<LibraryItem>> getTodayNewLibraryItems() {
    return Observable.fromFuture(http.get(
            'https://us-central1-insight-timer-a1ac7.cloudfunctions.net/apiHomePageNewToday/request?device_lang=en&content_langs=en'))
        .map((response) {
      if (response.statusCode == 200) {
        Map apiResp = json.decode(utf8.decode(response.bodyBytes));
        return apiResp["result"]["library_items_today"]
            .map<LibraryItem>((item) => LibraryItem.fromJson(item))
            .toList();
      } else {
        throw Exception(
            'Failed to load new today ${response.statusCode}: ${response.body}');
      }
    });
  }

  Observable<LibraryItem> getLibraryItemDetail(String id) {
    return Observable.fromFuture(http.get(
            'https://insight-timer-api.firebaseapp.com/apiLibraryItemGet/request/libraryItems/$id'))
        .map((response) {
      if (response.statusCode == 200) {
        return LibraryItem.fromJson(json.decode(response.body));
      } else {
        throw Exception(
            'Failed to load library item details information ${response.statusCode} ${response.body}');
      }
    });
  }

  Observable<LibraryItem> getDailyInsightItemDetails(String id) {
    return Observable.fromFuture(http.get(
            'https://insight-timer-api.firebaseapp.com/apiDailyInsightGet/request/dailyinsights/$id'))
        .map((response) {
      if (response.statusCode == 200) {
        return LibraryItem.fromJson(json.decode(response.body));
      } else {
        throw Exception(
            'Failed to load daily insight details information ${response.statusCode} ${response.body}');
      }
    });
  }

  Observable<List<LibraryItem>> getSleepLibraryItems() {
    return Observable.fromFuture(http.get(
            'https://insight-timer-api.firebaseapp.com/apiLibraryItemFilter/request?topics=naturesounds&content_langs=en&device_lang=en&limit=20'))
        .map((response) {
      if (response.statusCode == 200) {
        Map apiResp = json.decode(utf8.decode(response.bodyBytes));
        return apiResp["result"]
            .map<LibraryItem>((item) => LibraryItem.fromJson(item))
            .toList();
      } else {
        throw Exception(
            'Failed to load new today ${response.statusCode}: ${response.body}');
      }
    });
  }

  Observable<List<LibraryItem>> getFeaturedSleepLibraryItems() {
    return Observable.fromFuture(http.get(
            'https://insight-timer-api.firebaseapp.com/apiLibraryItemFilter/request?topics=sleep&content_langs=en&device_lang=en&limit=20'))
        .map((response) {
      if (response.statusCode == 200) {
        Map apiResp = json.decode(utf8.decode(response.bodyBytes));
        return apiResp["result"]
            .map<LibraryItem>((item) => LibraryItem.fromJson(item))
            .toList();
      } else {
        throw Exception(
            'Failed to load new today ${response.statusCode}: ${response.body}');
      }
    });
  }

  Observable<List<LibraryItem>> getSleepMusicItems(int limit, int offset) {
    return Observable.fromFuture(http.get(
            'https://insight-timer-api.firebaseapp.com/apiLibraryItemFilter/request?topics=sleep&content_langs=en&device_lang=en&limit=$limit&offset=$offset'))
        .map((response) {
      if (response.statusCode == 200) {
        Map apiResp = json.decode(utf8.decode(response.bodyBytes));
        return apiResp["result"]
            .map<LibraryItem>((item) => LibraryItem.fromJson(item))
            .toList();
      } else {
        throw Exception(
            'Failed to load new today ${response.statusCode}: ${response.body}');
      }
    });
  }
}

enum LibraryFilterRange {
  allLengths,
  zeroFive,
  sixTen,
  elevenFifteen,
  sixteenTwenty,
  twentyoneThirty,
  thirtyPlus,
}

enum LibraryFilterSorting {
  mostPlayed,
  highestRated,
  newest,
  shortest,
  longest,
}

enum LibraryFilterGenre {
  classical,
  chantingAndMantras,
  soundHealing,
  drumming,
  natureSounds,
  instrumental,
  ambientMusic,
  binauralBeats,
}

enum LibraryFilterAudioGender {
  male,
  female,
}

class LibraryFilter {
  int offset;
  int limit;
  LibraryFilterSorting sorting;
  bool hasBackgroundMusic;
  bool isReligious;
  bool isSpiritual;
  LibraryFilterAudioGender voiceGender;
  List<String> types;
  List<LibraryFilterGenre> genres;
  String language;
  List<String> contentLanguages;
  List<String> topics;
  LibraryFilterRange range = LibraryFilterRange.allLengths;

  LibraryFilter({
    this.offset,
    this.limit,
    this.sorting,
    this.hasBackgroundMusic,
    this.isReligious,
    this.isSpiritual,
    this.voiceGender,
    this.types,
    this.genres,
    this.language,
    this.contentLanguages,
    this.topics,
    this.range,
  });
}

class LibraryItemResult {
  LibraryFilter filter;
  List<LibraryItem> items;
  int total;
  LibraryItemResult({
    this.filter,
    this.items,
    this.total,
  });
}
