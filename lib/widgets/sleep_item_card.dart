import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:shimmer/shimmer.dart';

import '../model/library_item.dart';
import '../resource/insight_timer_icons_icons.dart';

String audioUrl =
    'http://staticmp3.insighttimer.com/high_quality/d6u4m7t7d1x1x8r7r5v9v4y3c5r4h6q5t7p1j9g6-v1.mp3';
final sleepChannel =
    MethodChannel('Flutter:sleep_Android:meditationFragment_channel');

class SleepItemCard extends StatefulWidget {
  final LibraryItem item;

  SleepItemCard(this.item);

  @override
  SleepItemCardState createState() {
    return SleepItemCardState();
  }
}

class SleepItemCardState extends State<SleepItemCard>
    with TickerProviderStateMixin {
//  static const openPlayerChannel =
//  MethodChannel('Flutter:sleep_Android:meditationFragment_channel');
  Animation<double> animation;

  AnimationController _controller;

  @override
  void initState() {
    super.initState();

    _controller = new AnimationController(
        vsync: this, duration: Duration(milliseconds: 100));
    animation = Tween(begin: 1.0, end: 0.95).animate(
        CurvedAnimation(parent: _controller, curve: Curves.bounceInOut));
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  void navigateToPlayer() {
    var inputData = <String, String>{
      "playableId": "q8g5x9r8k6c9k7x5s0k6p4q1y8t8h2r7j0q8r0u2",
      "imageUrl":
          "http://publicdata.insighttimer.com/media_data/rectangle_pictures/38514/medium/q8g5x9r8k6c9k7x5s0k6p4q1y8t8h2r7j0q8r0u2-v4.jpg"
    };
    sleepChannel.invokeMethod("openNativePlayer", inputData);

    //Do Something else...
//    Navigator.of(context, rootNavigator: true).push(
//      NoAnimationPageRoute(
//        builder: (BuildContext context) => AudioPlayerPage(
//              imgUrl: widget.item.picture.medium,
//              audioUrl: audioUrl,
//              id: widget.item.id,
//              theme: 'dark',
//            ),
//      ),
//    );
  }

  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width * 150.0 / 375.0;
    final height = width * 180.0 / 150.0;
    final blurHeight = height / 3;

    return GestureDetector(
      onTapDown: (_) {
        _controller.forward();
      },
      onTapUp: (_) {
        if (_controller.status == AnimationStatus.completed) {
          _controller.reverse().whenComplete(navigateToPlayer);
        } else {
          _controller.forward().whenComplete(
              () => _controller.reverse().whenComplete(navigateToPlayer));
        }
      },
      child: ScaleTransition(
        scale: animation,
        child: Container(
          width: width,
          height: height,
          child: Stack(
            overflow: Overflow.clip,
            children: <Widget>[
              Positioned.fill(
                child: Hero(
                  tag: widget.item.id,
                  child: DecoratedBox(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(8),
                      image: DecorationImage(
                        image: NetworkImage(widget.item.picture.medium),
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                ),
              ),
              Positioned.fill(
                child: Container(
                  alignment: Alignment.bottomCenter,
                  child: ClipPath(
                    clipper: TitlePath(),
                    child: Container(
                      height: blurHeight,
                      color: Colors.white.withOpacity(0.1),
                      padding: EdgeInsets.symmetric(horizontal: 11),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          SizedBox(height: 7),
                          Container(
                            height: 28,
                            child: Text(
                              widget.item.title,
                              style: TextStyle(fontSize: 12, height: 1.18),
                              maxLines: 2,
                              overflow: TextOverflow.ellipsis,
                            ),
                          ),
                          SizedBox(height: 9),
                          Row(
                            children: <Widget>[
                              Container(
                                width: width - 65,
                                child: Text(
                                  widget.item.publisher.name,
                                  style: TextStyle(
                                    fontSize: 10,
                                    color: Color(0x88ffffff),
                                  ),
                                  maxLines: 1,
                                  overflow: TextOverflow.ellipsis,
                                ),
                              ),
                              Expanded(child: Container()),
                              Icon(
                                InsightTimerIcons.star,
                                size: 12,
                                color: Colors.white.withOpacity(0.3),
                              ),
                              SizedBox(width: 4),
                              Text(
                                widget.item.rating_score.toStringAsFixed(1),
                                style: TextStyle(
                                  fontSize: 10,
                                  color: Colors.white.withOpacity(0.5),
                                ),
                              ),
                            ],
                          )
                        ],
                      ),
                    ),
                  ),
                ),
              ),
              Positioned.fill(
                child: Container(
                  alignment: Alignment.bottomLeft,
                  child: Container(
                    margin: EdgeInsets.only(left: 5, bottom: 74),
                    width: 48.0 / 150.0 * width,
                    height: 21.0 / 180.0 * height,
                    child: ClipPath(
                      clipper: TimePath(),
                      child: Container(
                        width: 48.0 / 150.0 * width,
                        height: 21.0 / 180.0 * height,
                        decoration: BoxDecoration(
                          color: Colors.grey.withOpacity(0.2),
                          borderRadius: BorderRadius.circular(6),
                        ),
                        child: Center(
                          child: Text(
                            (widget.item.media_length ~/ 60).toString() +
                                ' min',
                            style: TextStyle(
                              fontSize: 11,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class TitlePath extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    Path path = Path();
    path.lineTo(0.0, size.height - 6);
    path.quadraticBezierTo(0.0, size.height, 6, size.height);
    path.lineTo(size.width - 6, size.height);
    path.quadraticBezierTo(
      size.width,
      size.height,
      size.width,
      size.height - 6,
    );
    path.lineTo(size.width, 0.0);
    path.close();
    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) => false;
}

class TimePath extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    Path path = Path();
    path.moveTo(0, size.height / 2);
    path.quadraticBezierTo(0, size.height, 5, size.height);
    path.lineTo(size.width - 5, size.height);
    path.quadraticBezierTo(
      size.width,
      size.height,
      size.width,
      size.height / 2,
    );
    path.quadraticBezierTo(size.width, 0, size.width - 5, 0);
    path.lineTo(5, 0);
    path.quadraticBezierTo(0, 0, 0, size.height / 2);
    path.close();
    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) => false;
}

class SleepItemCardShimmer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width * 150.0 / 375.0;
    final height = width * 180.0 / 150.0;
    return Shimmer.fromColors(
      baseColor: Colors.white.withOpacity(0.05),
      highlightColor: Colors.white,
      child: Container(
        width: width,
        height: height,
        decoration: BoxDecoration(
          color: Colors.white.withOpacity(0.05),
          borderRadius: BorderRadius.circular(6),
        ),
      ),
    );
  }
}
