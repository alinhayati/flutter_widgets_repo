import 'package:flutter/material.dart';
import '../resource/insight_timer_icons_icons.dart';

import '../repositories/library_item.dart';
import '../widgets/sleep_item_card.dart';
import '../model/library_item.dart';

class SleepMusicPage extends StatefulWidget {
  _SleepMusicPageState createState() => _SleepMusicPageState();
}

class _SleepMusicPageState extends State<SleepMusicPage> {
  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;
    final backgroundHeight = width / 3 * 2;
    final buttonWidth = (width - 90) / 3;

    return Scaffold(
      backgroundColor: Color(0xff000000),
      body: Stack(
        children: <Widget>[
          AspectRatio(
            aspectRatio: 375 / 348,
            child: Image.asset(
              'assets/images/sleep_background.png',
              fit: BoxFit.cover,
            ),
          ),
          SingleChildScrollView(
            child: Column(
              children: <Widget>[
                Container(
                  margin: EdgeInsets.only(top: backgroundHeight / 2 - 20),
                  padding: EdgeInsets.symmetric(horizontal: 25),
                  height: backgroundHeight / 2 + 20,
                  width: double.infinity,
                  decoration: BoxDecoration(
                    gradient: LinearGradient(
                      begin: Alignment(0.5, 1),
                      end: Alignment(0.5, 0.6),
                      colors: [Color(0xff000000), Color(0x00000000)],
                    ),
                  ),
                  child: Column(
                    children: <Widget>[
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          InkWell(
                            child: Container(
                              width: 20,
                              height: 20,
                              child:
                                  Icon(InsightTimerIcons.arrow_left, size: 16),
                            ),
                            onTap: () => Navigator.pop(context),
                          ),
                          Text(
                            'Sleep Music',
                            style: TextStyle(
                              fontSize: 20,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          SizedBox(height: 16, width: 16),
                        ],
                      ),
                      SizedBox(height: 38),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Container(
                            margin: EdgeInsets.only(left: 10),
                            child: _buildButtom('Sort', buttonWidth),
                          ),
                          _buildButtom('Length', buttonWidth),
                          Container(
                            margin: EdgeInsets.only(right: 10),
                            child: _buildButtom('Filters', buttonWidth),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
                Container(
                  color: Color(0xff000000),
                  width: width,
                  child: MusicGrid(),
                ),
                SizedBox(height: 35),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildButtom(String buttonName, double buttonWidth) {
    return Material(
      color: Colors.transparent,
      child: InkWell(
        onTap: () {},
        child: Container(
          width: buttonWidth,
          height: 31,
          alignment: Alignment.center,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(6),
            color: Colors.white.withOpacity(0.1),
          ),
          child: Text(buttonName),
        ),
      ),
    );
  }
}

class MusicGrid extends StatefulWidget {
  _MusicGridState createState() => _MusicGridState();
}

class _MusicGridState extends State<MusicGrid> {
  List<Widget> gridViewItems;
  int offset = 0;
  int limit = 20;

  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
      stream: LibraryItemRepository().getSleepMusicItems(limit, offset),
      builder: (context, AsyncSnapshot<List<LibraryItem>> itemsSnapshot) {
        if (itemsSnapshot.hasError) {
          return Text(
            'Error',
            style: TextStyle(color: Colors.red, fontSize: 20),
          );
        }

        if (!itemsSnapshot.hasData) {
          gridViewItems =
              [0, 1, 2, 3].map((_) => SleepItemCardShimmer()).toList();
        } else {
          gridViewItems =
              itemsSnapshot.data.map((item) => SleepItemCard(item)).toList();
        }

        return GridView.count(
          shrinkWrap: true,
          physics: NeverScrollableScrollPhysics(),
          crossAxisCount: 2,
          mainAxisSpacing: 25,
          crossAxisSpacing: 25,
          childAspectRatio: 150 / 180,
          padding: EdgeInsets.symmetric(horizontal: 25),
          children: gridViewItems,
        );
      },
    );
  }
}
