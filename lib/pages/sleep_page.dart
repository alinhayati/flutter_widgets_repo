import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import './sleep_music_page.dart';
import '../model/library_item.dart';
import '../repositories/library_item.dart';
import '../resource/insight_timer_icons_icons.dart';
import '../widgets/sleep_item_card.dart';

//final sleepChannel =
//MethodChannel('Flutter:sleep_Android:meditationFragment_channel');

class SleepPage extends StatefulWidget {
  
  @override
  SleepPageState createState() {
    return SleepPageState();
  }
}

class SleepPageState extends State<SleepPage> {
  ScrollController _scrollController = ScrollController();

  double scrollPixels = 0;

  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
    _scrollController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;
    final backgroundHeight = width / 3 * 2;

    return Scaffold(
      backgroundColor: Color(0xff000000),
      body: Stack(
        children: <Widget>[
          AspectRatio(
            aspectRatio: 375 / 348,
            child: Image.asset(
              'assets/images/sleep_background.png',
              fit: BoxFit.cover,
            ),
          ),
          SingleChildScrollView(
            controller: _scrollController,
            child: Column(
              children: <Widget>[
                Container(
                  margin: EdgeInsets.only(top: backgroundHeight / 2 - 60),
                  height: backgroundHeight / 2 + 60,
                  width: width,
                  decoration: BoxDecoration(
                    gradient: LinearGradient(
                      begin: Alignment(0.5, 1),
                      end: Alignment(0.5, 0.6),
                      colors: [Color(0xff000000), Color(0x00000000)],
                    ),
                  ),
                  child: Column(
                    children: <Widget>[
                      Container(
                        width: 77,
                        height: 45,
                        child: Image.asset(
                          'assets/images/sleep_title.png',
                          fit: BoxFit.fill,
                        ),
                      ),
                      SizedBox(height: 7),
                      Text(
                        'Thousands of free music tracks and stories\nto help you get a better night’s sleep. ',
                        style: TextStyle(
                          fontSize: 12,
                          height: 1.4,
                          color: Color(0x88ffffff),
                        ),
                        textAlign: TextAlign.center,
                      ),
                    ],
                  ),
                ),
                Container(
                  color: Color(0xff000000),
                  width: width,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        margin: EdgeInsets.only(left: 25),
                        alignment: Alignment.bottomLeft,
                        child: Text(
                          'FEATURED',
                          style: TextStyle(
                            color: Color(0x88ffffff),
                            fontSize: 14,
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                      ),
                      SizedBox(height: 19),
                      _FeatureList(),
                      SizedBox(height: 39),
                      _TabItem('New'),
                      _TabItem('Stories'),
                      _TabItem('Meditations'),
                      _TabItem('Music'),
                      _TabItem('Soundscapes'),
                      _TabItem('Kids'),
                      SizedBox(height: 36),
                      Container(
                        margin: EdgeInsets.only(left: 25),
                        child: Text(
                          'POPULAR',
                          style: TextStyle(
                            color: Color(0x88ffffff),
                            fontSize: 13,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                      SizedBox(height: 22),
                      _PopularGrid(),
                      SizedBox(height: 35),
                    ],
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }

  Widget _blurredTop() {
    final sigma = scrollPixels / 3 < 30 ? scrollPixels / 3 : 30.0;
    return BackdropFilter(
      filter: new ImageFilter.blur(
        sigmaX: sigma,
        sigmaY: sigma,
      ),
      child: new Container(
        height: 400.0,
        decoration: new BoxDecoration(color: Colors.black.withOpacity(0.1)),
      ),
    );
  }
}

class _TabItem extends StatelessWidget {

  final String title;

  final Map sleepRoute = {
    "New": SleepMusicPage(),
    "Stories": SleepMusicPage(),
    "Meditations": SleepMusicPage(),
    'Music': SleepMusicPage(),
    'Soundscapes': SleepMusicPage(),
    'Kids': SleepMusicPage(),
  };

  _TabItem(this.title);

  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.transparent,
      child: InkWell(
//        onTap: _showNativeToast,
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: 25, vertical: 7.7),
          child: Row(
            children: <Widget>[
              Container(
                width: 28,
                height: 28,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(5),
                  color: Colors.white.withOpacity(0.1),
                ),
              ),
              SizedBox(width: 11),
              Text(
                title,
                style: TextStyle(
                  fontSize: 16,
                  color: Colors.white.withOpacity(0.4),
                ),
              ),
              Expanded(child: Container()),
              Icon(
                InsightTimerIcons.arrow_right_thin,
                size: 14,
                color: Colors.white.withOpacity(0.4),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Future<Null> _showNativeToast() async {
//    await sleepChannel.invokeMethod('showNativeToast');
  }
}

class _FeatureList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width * 150.0 / 375.0;
    final height = width * 180.0 / 150.0;
    return StreamBuilder(
      stream: LibraryItemRepository().getSleepLibraryItems(),
      builder: (context, AsyncSnapshot<List<LibraryItem>> itemsSnapshot) {
        if (itemsSnapshot.hasError) {
          return Text(
            'Error',
            style: TextStyle(color: Colors.red, fontSize: 20),
          );
        }

        List<Widget> listItems;

        if (!itemsSnapshot.hasData) {
          listItems = [SizedBox(width: 20)]..addAll([0, 1, 2]
              .map((_) => Container(
                  margin: EdgeInsets.only(right: 11.5),
                  child: SleepItemCardShimmer()))
              .toList());
        } else {
          listItems = [SizedBox(width: 20)]..addAll(itemsSnapshot.data
              .map((item) => Container(
                    margin: EdgeInsets.only(right: 11.5),
                    child: SleepItemCard(item),
                  ))
              .toList());
        }

        return Container(
          height: height,
          child: ListView(
            scrollDirection: Axis.horizontal,
            children: listItems,
          ),
        );
      },
    );
  }
}

class _PopularGrid extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
      stream: LibraryItemRepository().getFeaturedSleepLibraryItems(),
      builder: (context, AsyncSnapshot<List<LibraryItem>> itemsSnapshot) {
        if (itemsSnapshot.hasError) {
          return Text(
            'Error',
            style: TextStyle(color: Colors.red, fontSize: 20),
          );
        }

        List<Widget> gridViewItems;

        if (!itemsSnapshot.hasData) {
          gridViewItems =
              [0, 1, 2, 3].map((_) => SleepItemCardShimmer()).toList();
        } else {
          gridViewItems = itemsSnapshot.data
              .map(
                (item) => SleepItemCard(item),
              )
              .toList();
        }

        return GridView.count(
          shrinkWrap: true,
          physics: NeverScrollableScrollPhysics(),
          crossAxisCount: 2,
          mainAxisSpacing: 21,
          crossAxisSpacing: 27,
          childAspectRatio: 150.0 / 180.0,
          padding: EdgeInsets.symmetric(horizontal: 25),
          children: gridViewItems,
        );
      },
    );
  }
}
